package com.products.microserviceproducts.dto;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Types;
import java.util.UUID;
@AllArgsConstructor
@Getter
@Setter
public class ProductDto {
    private long id;
    private String name;
    private String description;
    private double price;
}
