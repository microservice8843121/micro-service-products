package com.products.microserviceproducts.controller;

import com.products.microserviceproducts.dto.ProductDto;
import com.products.microserviceproducts.entity.Product;
import com.products.microserviceproducts.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/products")
@AllArgsConstructor
public class ProductController {
    private ProductService productService;
    @GetMapping
    public ResponseEntity<List<Product>> getAll(){
        System.out.println("Products getAll");
        List<Product> books = productService.getAll();
        System.out.println(books);
        return ResponseEntity.status(HttpStatus.OK).body(books);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getById(@PathVariable long id){
        Product product = productService.getById(id);
        return  ResponseEntity.status(HttpStatus.OK).body(product);
    }
    @PostMapping
    public ResponseEntity<Product> insert(@RequestBody ProductDto dto) {
        System.out.println("Insert");
        Product bookSaved = productService.insert(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(bookSaved);
    }

    @PutMapping
    public ResponseEntity<Product> update( @RequestBody ProductDto dto) {
        System.out.println("Update");
        System.out.println("id: "+dto.getId());
        Product bookSaved = productService.update(dto.getId(), dto);
        return ResponseEntity.status(HttpStatus.OK).body(bookSaved);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        System.out.println("Delete");
        productService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
