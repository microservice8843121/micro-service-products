package com.products.microserviceproducts.service;

import com.products.microserviceproducts.dto.ProductDto;
import com.products.microserviceproducts.entity.Product;
import com.products.microserviceproducts.exception.EntityNotFoundException;
import com.products.microserviceproducts.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService{
    private ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public Product getById(long id) {
        System.out.println("GetById: "+ id);
        return productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Products", id));
    }

    @Override
    public Product insert(ProductDto dto) {
        Product product = new Product();
        product.setName(dto.getName());
        product.setDescription(dto.getDescription());
        product.setPrice(dto.getPrice());
        return productRepository.save(product);
    }

    @Override
    public Product update(long id, ProductDto dto) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Prodcut", id));
        product.setName(dto.getName());
        product.setDescription(dto.getDescription());
        product.setPrice(dto.getPrice());
        return productRepository.save(product);
    }

    @Override
    public void delete(long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Prodcut", id));
        productRepository.delete(product);
    }
}
