package com.products.microserviceproducts.service;

import com.products.microserviceproducts.dto.ProductDto;
import com.products.microserviceproducts.entity.Product;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    List<Product> getAll();
    Product getById(long id);
    Product insert(ProductDto dto);
    Product update(long id, ProductDto dto);
    void delete(long id);
}
