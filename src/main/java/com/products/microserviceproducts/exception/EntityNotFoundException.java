package com.products.microserviceproducts.exception;
import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String entity, long id) {
        super(String.format("%s with id: %x not found.", entity, id));
    }
}
