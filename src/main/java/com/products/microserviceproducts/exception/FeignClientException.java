package com.products.microserviceproducts.exception;

import lombok.Getter;

@Getter
public class FeignClientException extends RuntimeException {
    private final Object errorResponse;
    private final Integer status;

    public FeignClientException(Object errorResponse, Integer status) {
        this.errorResponse = errorResponse;
        this.status = status;
    }
}