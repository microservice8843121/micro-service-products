package com.products.microserviceproducts.entity;

import jakarta.persistence.*;

import java.sql.Types;
import java.time.LocalDate;
import java.util.UUID;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.id.factory.internal.AutoGenerationTypeStrategy;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    private Double price;


    /*@ManyToOne
    @JoinColumn(name = "id_author")
    private Author author;*/
}

