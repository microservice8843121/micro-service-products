package com.products.microserviceproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class MicroServiceProductsApplication {
	public static void main(String[] args) {
		SpringApplication.run(MicroServiceProductsApplication.class, args);
		System.out.println("*** RUN microservice-products  ***");
	}
}
