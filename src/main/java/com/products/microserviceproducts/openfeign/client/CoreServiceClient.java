package com.products.microserviceproducts.openfeign.client;

import com.products.microserviceproducts.config.openfeign.OpenFeignConfig;
import com.products.microserviceproducts.openfeign.pojo.PersonBasicInfoPojo;
import com.products.microserviceproducts.openfeign.pojo.ProductPojo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "${core.service.name}", configuration = OpenFeignConfig.class)
public interface CoreServiceClient {

    @GetMapping("${core.service.context-path}persons/getPersonBasicInfoById/{id}")
    PersonBasicInfoPojo getPersonBasicInfoById(@PathVariable Integer id);

}
