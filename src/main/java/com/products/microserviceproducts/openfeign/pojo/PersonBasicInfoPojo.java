package com.products.microserviceproducts.openfeign.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Pojo
public class PersonBasicInfoPojo {
  private Integer id;
  private String documentNumber;
  private String personFullName;
}
