package com.products.microserviceproducts.openfeign.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import java.util.UUID;


@AllArgsConstructor
@Getter
@Setter
public class ProductPojo {
    private UUID id;
    private String name;
    private String description;
    private Double price;
}
