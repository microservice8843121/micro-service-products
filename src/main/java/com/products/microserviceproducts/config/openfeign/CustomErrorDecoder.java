package com.products.microserviceproducts.config.openfeign;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.products.microserviceproducts.exception.ErrorResponse;
import com.products.microserviceproducts.exception.FeignClientException;
import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.http.HttpStatus;

import java.io.IOException;

public class CustomErrorDecoder implements ErrorDecoder {

    private final ObjectMapper objectMapper;

    public CustomErrorDecoder(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Exception decode(String methodKey, Response response) {
        System.out.println("decode");
        HttpStatus httpStatus = HttpStatus.valueOf(response.status());
        Object errorResponse;
        if (httpStatus.is4xxClientError() || httpStatus.is5xxServerError()) {
            if (httpStatus.is5xxServerError()) {
                errorResponse = handle5xxServerError(httpStatus, response);
            } else {
                errorResponse = handle4xxClientError(httpStatus, response);
                httpStatus = HttpStatus.BAD_REQUEST;
            }
        } else {
            return FeignException.errorStatus(methodKey, response);
        }
        return new FeignClientException(errorResponse, httpStatus.value());

    }

    private Object handle4xxClientError(HttpStatus httpStatus, Response response) {
        return createErrorResponse(httpStatus, response, "Error without body", "Unknown path");
    }

    private Object handle5xxServerError(HttpStatus httpStatus, Response response) {
        String errorMessage = (httpStatus.value() == 503) ? "Server Error" : "Error without body";
        return createErrorResponse(httpStatus, response, errorMessage, (httpStatus.value() == 503) ? response.request().url() : "Unknown path");
    }

    private Object createErrorResponse(HttpStatus httpStatus, Response response, String message, String path) {
        Object errorResponse = readValue(response);
        return (errorResponse != null) ? errorResponse : new ErrorResponse(httpStatus.value(), httpStatus.getReasonPhrase(), message, path);
    }

    private <T> T readValue(Response response) {
        try {
            return objectMapper.readValue(response.body().asInputStream(), (Class<T>) Object.class);
        } catch (IOException e) {
            return null;
        }
    }
}
