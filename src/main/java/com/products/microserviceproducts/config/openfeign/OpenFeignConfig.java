package com.products.microserviceproducts.config.openfeign;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenFeignConfig {

    @Bean
    public ErrorDecoder errorDecoder() {
        System.out.println("ErrorDecoder");
        ObjectMapper objectMapper = new ObjectMapper();
        return new CustomErrorDecoder(objectMapper);
    }
}