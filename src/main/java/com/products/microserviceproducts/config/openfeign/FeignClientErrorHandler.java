package com.products.microserviceproducts.config.openfeign;


import com.products.microserviceproducts.exception.ErrorResponse;
import com.products.microserviceproducts.exception.FeignClientException;
import feign.FeignException;
import org.springframework.http.HttpStatus;

public class FeignClientErrorHandler {
    public static void handleFeignException(FeignException feignException) {
        System.out.println("handleFeignException");
        HttpStatus status = (feignException.status() == -1) ? HttpStatus.SERVICE_UNAVAILABLE : HttpStatus.valueOf(feignException.status());
        String origin = (feignException.request() != null) ? feignException.request().url() : "Unknown endpoint";
        ErrorResponse errorResponse = new ErrorResponse(status.value(), status.name(), "Server Error", origin);
        throw new FeignClientException(errorResponse, status.value());
    }
}
